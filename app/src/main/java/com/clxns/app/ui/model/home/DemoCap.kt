package com.clxns.app.ui.model.home

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class DemoCap(
    @Json(name = "mainSupporting")
    var mainSupporting : ArrayList<String>? = ArrayList()
)