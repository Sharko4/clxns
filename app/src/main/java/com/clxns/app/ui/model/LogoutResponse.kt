package com.clxns.app.ui.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class LogoutResponse(

	@Json(name="title")
	val title: String? = null,

	@Json(name="error")
	val error: Boolean? = null
)
