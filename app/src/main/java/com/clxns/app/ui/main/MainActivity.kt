package com.clxns.app.ui.main

import android.content.Intent
import android.content.IntentSender.SendIntentException
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.widget.Switch
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.view.GestureDetectorCompat
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.clxns.app.R
import com.clxns.app.data.api.helper.NetworkResult
import com.clxns.app.data.database.BankDetailsEntity
import com.clxns.app.data.database.DispositionEntity
import com.clxns.app.data.database.SubDispositionEntity
import com.clxns.app.data.preference.SessionManager
import com.clxns.app.databinding.ActivityMainBinding
import com.clxns.app.ui.login.LoginActivity
import com.clxns.app.ui.main.account.AccountViewModel
import com.clxns.app.utils.Constants
import com.clxns.app.utils.Constants.APP_UPDATE_RC
import com.clxns.app.utils.snackBar
import com.clxns.app.utils.toast
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.UpdateAvailability
import com.google.android.play.core.tasks.Task
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import javax.inject.Inject


@AndroidEntryPoint
class MainActivity : AppCompatActivity(),
    GestureDetector.OnGestureListener {

    private lateinit var ourDetector: GestureDetectorCompat
    private var timeset: Long = 1000
    private var noOfClicks: Int = 0
    private var isActive: Boolean = false

    private lateinit var binding : ActivityMainBinding

    private val mainViewModel : MainViewModel by viewModels()

    internal lateinit var user:SwitchCompat






    @Inject
    lateinit var sessionManager : SessionManager

    private lateinit var appUpdateManager : AppUpdateManager

    private lateinit var navController : NavController
    private lateinit var snackBar : Snackbar





      override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)

        initView()

        subscribeObserver()

        checkForAppUpdate()
    }

    private fun initView() {
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        user = findViewById<View>(R.id.user) as SwitchCompat

        user.setOnClickListener(View.OnClickListener {
            if (user.isChecked)
            {
                Toast.makeText(applicationContext,"User is Online",Toast.LENGTH_SHORT).show()
            }
            else
            {
                Toast.makeText(applicationContext,"User is Offline",Toast.LENGTH_SHORT).show()
            }
        })

        subscribeObserver()



          ourDetector = GestureDetectorCompat(this, this)



        val token = sessionManager.getString(Constants.TOKEN).toString()
        val navView : BottomNavigationView = binding.navView

        navController = findNavController(R.id.nav_host_fragment_activity_main)

        snackBar = Snackbar.make(binding.root, "Press back again to exit", Snackbar.LENGTH_LONG)
        snackBar.animationMode = BaseTransientBottomBar.ANIMATION_MODE_SLIDE

        navView.setOnItemReselectedListener { }

        navView.setupWithNavController(navController)

        //Timber.i("MAIN_TOKEN -> ${sessionManager.getString(Constants.TOKEN)!!}")

        mainViewModel.getAllDispositions()
        mainViewModel.getBankList(token)
    }

    override fun onNewIntent(intent : Intent?) {
        super.onNewIntent(intent)
        /**
         * This will refresh the my plan fragment only once after successful payment
         * It only gets triggered from the payment activity by clearing back stack
         * up to this activity as it's been Launched in Single Top mode (Only One Instance)
         */
        if (intent != null) {
            if (intent.getBooleanExtra("hasChangedPlanStatus", false)) {
                setIntent(intent)
            }
        }
    }

    private fun subscribeObserver() {
        mainViewModel.responseBankList.observe(this) {
            when (it) {
                is NetworkResult.Success -> {
                    if (it.data?.error == false) {
                        val bankList = arrayListOf<BankDetailsEntity>()
                        for (data in it.data.bankData) {
                            bankList.add(
                                BankDetailsEntity(
                                    data.id,
                                    data.name,
                                    data.location,
                                    data.category,
                                    data.description,
                                    Constants.BANK_LOGO_URL + data.fiImage
                                )
                            )
                        }
                        mainViewModel.saveAllBankDetails(bankList)
                    }
                }
                is NetworkResult.Loading -> {
                }
                is NetworkResult.Error -> Timber.i(it.message)
            }
        }
        mainViewModel.responseDisposition.observe(this) {
            when (it) {
                is NetworkResult.Success -> {
                    //Timber.i(it.data.toString())
                    if (it.data?.error == false) {
                        if (it.data.dispositionData.isNotEmpty()) {
                            val dispositionList = arrayListOf<DispositionEntity>()
                            val subDispositionList = arrayListOf<SubDispositionEntity>()
                            for (dis in it.data.dispositionData) {
                                dispositionList.add(DispositionEntity(dis.id, dis.name))
                                if (dis.subDispositionDataList.isNotEmpty()) {
                                    for (subDis in dis.subDispositionDataList) {
                                        subDispositionList.add(
                                            SubDispositionEntity(
                                                subDis.id,
                                                subDis.name,
                                                subDis.dispositionId
                                            )
                                        )
                                    }
                                }
                            }
                            mainViewModel.saveAllDispositions(dispositionList)
                            mainViewModel.saveAllSubDispositions(subDispositionList)
                        }
                    }
                }
                is NetworkResult.Loading -> {
                    Timber.i("Loading...")
                }
                is NetworkResult.Error -> Timber.i("Error")
            }
        }
        mainViewModel.responseLogout.observe(this) { response ->
            when (response) {
                is NetworkResult.Success -> {
                    // update UI
                    toast(response.data?.title!!)
                    if (response.data.error == false) {
                        //Removing authentication token
                        sessionManager.removeData(Constants.IS_USER_LOGGED_IN)
                        sessionManager.removeData(Constants.TOKEN)
                        //start login screen
                        val intent = Intent(this, LoginActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP
                        startActivity(intent)
                        finish()
                    }
                    // bind data to the view
                }
                is NetworkResult.Error -> {
                    // show error message
                    binding.root.snackBar(response.message!!)
                }
                is NetworkResult.Loading -> {
                    binding.root.snackBar("Logging out...")
                }
            }
        }
    }


    private fun checkForAppUpdate() {
        appUpdateManager = AppUpdateManagerFactory.create(this)
        // Returns an intent object that you use to check for an update.
        val appUpdateInfoTask : Task<AppUpdateInfo> = appUpdateManager.appUpdateInfo
        // Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener { appUpdateInfo ->
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)
            ) {
                try {
                    appUpdateManager.startUpdateFlowForResult(
                        appUpdateInfo,
                        AppUpdateType.IMMEDIATE,
                        this,
                        APP_UPDATE_RC
                    )
                } catch (e : SendIntentException) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onActivityResult(requestCode : Int, resultCode : Int, data : Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == APP_UPDATE_RC) {
            if (resultCode != RESULT_OK) {
                showUpdateAvailableDialog()
            }
        }
    }

    private fun showUpdateAvailableDialog() {
        AlertDialog.Builder(this).apply {
            setTitle("Update Available!!")
            setMessage("This update is necessary for the app to work properly. Please initiate the update, it'll only take few minutes.")
            setCancelable(false)
            setPositiveButton(
                "Update Now"
            ) { dialog, _ ->
                checkForAppUpdate()
                dialog.dismiss()
            }
            setNegativeButton("Exit") { dialog, _ ->
                finish()
                dialog.dismiss()
            }
        }.create().show()
    }

    override fun onResume() {
        super.onResume()
        if (::appUpdateManager.isInitialized) {
            appUpdateManager
                .appUpdateInfo
                .addOnSuccessListener { appUpdateInfo ->
                    if (appUpdateInfo.updateAvailability()
                        == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS
                    ) {
                        // If an in-app update is already running, resume the update.
                        appUpdateManager.startUpdateFlowForResult(
                            appUpdateInfo,
                            AppUpdateType.IMMEDIATE,
                            this,
                            APP_UPDATE_RC
                        )
                    }
                }
        }
    }

    override fun onBackPressed() {
        if (navController.currentDestination!!.id == navController.graph.startDestinationId) {
            if (snackBar.isShown) {
                super.onBackPressed()
            } else {
                snackBar.show()
            }
        } else {
            super.onBackPressed()
        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return if (ourDetector.onTouchEvent(event)) {
            true
        } else {
            super.onTouchEvent(event)
        }
    }

    override fun onDown(event: MotionEvent): Boolean {
        startDetection()
        return true
    }

    override fun onFling(
        event1: MotionEvent,
        event2: MotionEvent,
        velocityX: Float,
        velocityY: Float
    ): Boolean {
        startDetection()
        return true
    }

    override fun onLongPress(event: MotionEvent) {
        startDetection()
    }

    override fun onScroll(
        event1: MotionEvent,
        event2: MotionEvent,
        distanceX: Float,
        distanceY: Float
    ): Boolean {
        startDetection()
        return true
    }

    override fun onShowPress(event: MotionEvent) {
        startDetection()
    }

    override fun onSingleTapUp(event: MotionEvent): Boolean {
        startDetection()
        return true
    }

    //a method to start detection
    fun startDetection() {
        //set the user's active status to true then increase the number of clicks by 1
        isActive = true
        noOfClicks++

        //dont listen more than once
        //simply do nothing :)
        if (noOfClicks > 1) {

        } else if (noOfClicks == 1) {
            startListener()
        }
    }

    fun startListener() {
        //check the user's activeness after a specified time in milliseconds
        Handler(Looper.getMainLooper()).postDelayed({
            //displayDialog()
            checkActiveness()
        }, timeset)
    }

    fun checkActiveness() {
        /*set the active status to false deliberately
        and wait for 5 seconds to see if it will change to true
        */
        isActive = false
        Handler(Looper.getMainLooper()).postDelayed({

            displayDialog()
        }, 10000)
    }

     fun displayDialog() {
        //if the user is still inactive, show the dialog
        /*reset the number of clicks to zero to start
        the detection  incase the dialog is dismissed*/
        if (!isActive) {
            noOfClicks = 0
            val displayDialog = android.app.AlertDialog.Builder(this)
            displayDialog.apply {
                setTitle("Auto logout")
                setMessage("It seems you have you have been away for a while. Would you like us to sign you out?")
                setPositiveButton("Yes, Sign out") { dialog, _ ->


                    sessionManager.getString(Constants.TOKEN)?.let {
                        mainViewModel.logout(it)
                    }
                    dialog.dismiss()


                }
                setNegativeButton("Keep me in") { dialog, _ -> dialog.dismiss()

                }

            }.create().show()
        }
        else
        {
            /*if active, set the clicks to zero also
            to allow the detection to start as the user clicks/taps the screen
             */
            noOfClicks = 0
        }
    }

}