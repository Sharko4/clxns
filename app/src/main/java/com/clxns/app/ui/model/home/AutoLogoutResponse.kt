package com.clxns.app.ui.model.home

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AutoLogoutResponse(

    @Json(name="title")
    val title: String? = null,

    @Json(name="error")
    val error: Boolean? = null
)