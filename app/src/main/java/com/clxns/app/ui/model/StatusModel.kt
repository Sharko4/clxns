package com.clxns.app.ui.model

import android.graphics.drawable.Drawable

data class StatusModel(var status: String, var icon: Drawable?)