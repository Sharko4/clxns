package com.clxns.app.data.repository

import com.clxns.app.data.api.helper.BaseApiResponse
import com.clxns.app.data.api.helper.NetworkResult
import com.clxns.app.data.api.helper.RemoteDataSource
import com.clxns.app.data.database.BankDetailsEntity
import com.clxns.app.data.database.DispositionEntity
import com.clxns.app.data.database.LocalDataSource
import com.clxns.app.data.database.SubDispositionEntity
import com.clxns.app.ui.model.DispositionResponse
import com.clxns.app.ui.model.FISBankResponse
import com.clxns.app.ui.model.LogoutResponse
import dagger.hilt.android.scopes.ActivityRetainedScoped
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

@ActivityRetainedScoped
class MainRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource,
    private val localDataSource: LocalDataSource
) : BaseApiResponse() {

    suspend fun getAllDispositions(): Flow<NetworkResult<DispositionResponse>> {
        return flow<NetworkResult<DispositionResponse>> {
            emit(safeApiCall {
                remoteDataSource.getAllDispositions()
            })
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getBankList(token: String): Flow<NetworkResult<FISBankResponse>> {
        return flow<NetworkResult<FISBankResponse>> {
            emit(safeApiCall {
                remoteDataSource.getBankList(token)
            })
        }.flowOn(Dispatchers.IO)
    }

    suspend fun saveAllDispositions(dispositionList: List<DispositionEntity>) =
        localDataSource.saveAllDispositions(dispositionList)

    suspend fun saveAllSubDispositions(subDispositionList: List<SubDispositionEntity>) =
        localDataSource.saveAllSubDispositions(subDispositionList)

    suspend fun saveAllBankDetails(bankDetailList: List<BankDetailsEntity>) =
        localDataSource.saveAllBankDetails(bankDetailList)

    suspend fun logout(token : String) : Flow<NetworkResult<LogoutResponse>> {
        return flow<NetworkResult<LogoutResponse>> {
            emit(safeApiCall { remoteDataSource.logout(token) })
        }.flowOn(Dispatchers.IO)
    }

}